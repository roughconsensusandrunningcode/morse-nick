from st3m.application import Application, ApplicationContext
from st3m.ui.colours import WHITE, BLACK  
from st3m.input import InputState
from ctx import Context
import leds
import st3m.run

import json
import math


class Configuration:
    def __init__(self) -> None:
        self.name = "flow3r"
        self.size: int = 75
        self.font: int = 5

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                jsondata = f.read()
            data = json.loads(jsondata)
        except OSError:
            data = {}
        if "name" in data and type(data["name"]) == str:
            res.name = data["name"]
        if "size" in data:
            if type(data["size"]) == float:
                res.size = int(data["size"])
            if type(data["size"]) == int:
                res.size = data["size"]
        if "font" in data and type(data["font"]) == int:
            res.font = data["font"]
        return res

    def save(self, path: str) -> None:
        d = {
            "name": self.name,
            "size": self.size,
            "font": self.font,
        }
        jsondata = json.dumps(d)
        with open(path, "w") as f:
            f.write(jsondata)
            f.close()

class MorseEncoder:
    _morse_code = { 'A':'.-', 'B':'-...', 'C':'-.-.', 'D':'-..',
                    'E':'.', 'F':'..-.', 'G':'--.', 'H':'....',
                    'I':'..', 'J':'.---', 'K':'-.-', 'L':'.-..',
                    'M':'--', 'N':'-.', 'O':'---', 'P':'.--.',
                    'Q':'--.-', 'R':'.-.', 'S':'...', 'T':'-',
                    'U':'..-', 'V':'...-', 'W':'.--', 'X':'-..-',
                    'Y':'-.--', 'Z':'--..',
                    '1':'.----', '2':'..---', '3':'...--',
                    '4':'....-', '5':'.....', '6':'-....',
                    '7':'--...', '8':'---..', '9':'----.',
                    '0':'-----', ', ':'--..--', '.':'.-.-.-',
                    '?':'..--..', '/':'-..-.', '-':'-....-',
                    '(':'-.--.', ')':'-.--.-'}
    
    def encode(self, message):
        morse = ''
        message = message.upper()
        for letter in message:
            if letter != ' ' and letter in MorseEncoder._morse_code:
                 morse += MorseEncoder._morse_code[letter] + ' '
            else:
                morse += '/'
        return morse
    
class MorseNickApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        
        # https://www.codebug.org.uk/learn/step/541/morse-code-timing-rules/
        # The length of a dot is 1 time unit.
        # A dash is 3 time units.
        # The space between symbols (dots and dashes) of the same letter is 1 time unit.
        # The space between letters is 3 time units.
        # The space between words is 7 time units.
        time_unit = 250
        intervals = {
            '.': (1 * time_unit, WHITE),
            '-': (3 * time_unit, WHITE),
            ' ': (3 * time_unit, BLACK),
            '/': (7 * time_unit, BLACK)
        }

        self._led = 0.0
        self._filename = "/flash/nick.json"
        self._config = Configuration.load(self._filename)
        encoder = MorseEncoder()
        encoded_nick = encoder.encode(self._config.name)
        self._intervals = []
        
        for c in encoded_nick:
            self._intervals.append(intervals[c])
            self._intervals.append((time_unit, BLACK)) # space between symbols
        self._intervals.append((6 * time_unit, BLACK))

        self._curr_interval = 0
        self._time = 0
        self._scale = 1.0
        self._phase = 0.0


    def draw(self, ctx: Context) -> None:
        leds.set_hsv(int(self._led), abs(self._scale) * 360, 1, 0.2)
        leds.update()
        
        ctx.rgb(*self._intervals[self._curr_interval][1]).rectangle(-120, -120, 240, 240).fill()
        # ctx.fill()

    def on_exit(self) -> None:
        self._config.save(self._filename)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._phase += delta_ms / 1000
        self._scale = math.sin(self._phase)
        self._led += delta_ms / 45
        if self._led >= 40:
            self._led = 0

        self._time += delta_ms
        if self._time > self._intervals[self._curr_interval][0]:
            self._time = 0
            self._curr_interval = (self._curr_interval + 1) % len(self._intervals)


if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(MorseNickApp(ApplicationContext()))
